package com.potingchiang.android.modules.loginbase;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import popup_window.PopupWindowBase;

/**
 * Interface for login base
 * Components references
 * ImageButton mImgBtnCancelThumb;
 * TextView mTxtTitle;
 * EditText mEditIdInput;
 * EditText mEditPasswordInput;
 * ImageButton mImgBtnLogin;
 * ImageButton mImgBtnLogout;
 * ImageButton mImgBtnCancel;
 *
 * could be modified for more generalized use
 *
 * Created by potingchiang on 2017-11-06.
 */

public interface LoginBase {

    // components - getters & setters
    // ImageButton mImgBtnCancelThumb
    ImageButton getImgBtnCancelThumb();

    // ImageButton mImgBtnLogin;
    ImageButton getImgBtnLogin();

    // ImageButton mImgBtnLogout;
    ImageButton getImgBtnLogout();

    // ImageButton mImgBtnCancel;
    ImageButton getImgBtnCancel();

    // TextView mTxtTitle;
    TextView getTxtTitle();

    // EditText mEditIdInput;
    EditText getEditTxtIdInput();

    // EditText mEditPasswordInput;
    EditText getEditTxtPasswordInput();

    PopupWindowBase getParentPopupBase();

}

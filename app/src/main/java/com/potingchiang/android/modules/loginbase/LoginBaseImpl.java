package com.potingchiang.android.modules.loginbase;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import popup_window.AbsPopupWindowBaseLayoutInitiator;
import popup_window.PopupWindowBase;

/**
 * Class for login base interface implementation
 * Need to override onClick method when to instantiate new instance
 * for more components click listeners
 *
 * Created by potingchiang on 2017-11-06.
 */

public class LoginBaseImpl extends AbsPopupWindowBaseLayoutInitiator
        implements LoginBase, View.OnClickListener {

    private final   View        mView;
    private         ImageButton mImgBtnCancelThumb;
    private         TextView    mTxtTitle;
    private         EditText    mEditIdInput;
    private         EditText    mEditPasswordInput;
    private         ImageButton mImgBtnLogin;
    private         ImageButton mImgBtnLogout;
    private         ImageButton mImgBtnCancel;

    public LoginBaseImpl(Context context) {
        super(context, R.layout.login_base, null, false);

        mView           = getParentView();
    }

    @Override
    public void initLayoutComponents() {

        // init components
        // ImageButton - img_btn_cancel_thumb
        mImgBtnCancelThumb  = mView.findViewById(R.id.img_btn_cancel_thumb);
        // TextView - txt_title
        mTxtTitle           = mView.findViewById(R.id.txt_title);
        // EditText - edit_txt_id_input
        mEditIdInput        = mView.findViewById(R.id.edit_txt_id_input);
        // EditText - edit_text_password_input
        mEditPasswordInput  = mView.findViewById(R.id.edit_text_password_input);
        // ImageButton - img_btn_login
        mImgBtnLogin        = mView.findViewById(R.id.img_btn_login);
        // ImageButton - img_btn_logout
        mImgBtnLogout       = mView.findViewById(R.id.img_btn_logout);
        // ImageButton - img_btn_cancel
        mImgBtnCancel       = mView.findViewById(R.id.img_btn_cancel);
    }
    @Override
    public void initClickListeners() {

        // dismiss popup
        mImgBtnCancelThumb.setOnClickListener(this);
        mImgBtnLogin.setOnClickListener(this);
        mImgBtnLogout.setOnClickListener(this);
        mImgBtnCancel.setOnClickListener(this);
        mTxtTitle.setOnClickListener(this);
        mEditIdInput.setOnClickListener(this);
        mEditPasswordInput.setOnClickListener(this);
    }

    @Override
    public ImageButton getImgBtnCancelThumb() {
        return mImgBtnCancelThumb;
    }

    @Override
    public ImageButton getImgBtnLogin() {
        return mImgBtnLogin;
    }

    @Override
    public ImageButton getImgBtnLogout() {
        return mImgBtnLogout;
    }

    @Override
    public ImageButton getImgBtnCancel() {
        return mImgBtnCancel;
    }

    @Override
    public TextView getTxtTitle() {
        return mTxtTitle;
    }

    @Override
    public EditText getEditTxtIdInput() {
        return mEditIdInput;
    }

    @Override
    public EditText getEditTxtPasswordInput() {
        return mEditPasswordInput;
    }

    @Override
    public PopupWindowBase getParentPopupBase() {
        return this;
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.img_btn_cancel_thumb || v.getId() == R.id.img_btn_cancel) {

            dismissPopup();
        }
    }
}
